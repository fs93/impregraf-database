app.factory('resultServices', ['$http', function($http){
	var postApi = 'API/post.php';
	var getApi  = 'API/get.php';

	var methods = {

		//INDEX
		getClients: function(cb){
			obj = "mostrar";
			$http.post('API/index.php', obj).then(function(result, err){
				cb(result.data);
			});
		},

		// POST METHODS

		delete: function(obj, cb){
			$http.post(postApi, obj).then(function(result){
				cb(result);
			});
		},

		edit: function(obj, cb){
			$http.post(postApi, obj).then(function(result, err){
				if (!err) {
					cb(result);
				}
			});
		},

		new: function(obj, cb){
			$http.post(postApi, obj).then(function(result, err){
				if (!err) {
					cb(result);
				}
			})
		},

		saveForm: function(obj, cb){
			$http.post(postApi, obj).then(function(result, err){
				console.log(result, err);
			})
		},

		// GET'S METHODS

		getClientById: function(obj, cb){
			$http.post(getApi, obj).then(function(result, err){
				console.log(result, err);
				cb(result.data);
			})
		},

		getWorkById: function(obj, cb){
			console.log(obj);
			$http.post(getApi, obj).then(function(result, err){
				console.log("Service result:", result, err);
				cb(result);
			})
		}
	}
	return methods;
}]);
