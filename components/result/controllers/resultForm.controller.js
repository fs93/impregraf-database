app.controller('formCtrl', ['$scope', '$rootScope', 'resultServices', 'ngDialog', '$state',  function($scope, $rootScope, resultServices, ngDialog, $state) {

$scope.form = function (id){
    $rootScope.obj = {
        id: id,
        filter: "id"
    }
}

resultServices.getClientById($rootScope.obj, function(result, err){
    if (!err) {
        $scope.cliente = result[0];
        console.log($scope.cliente);
    }
});
    
$scope.workState = {
	1:"En proceso", 2:"Terminado", 3:"Esperando constancia"
};
$scope.selected = {value: $scope.workState[0]};

$scope.workType = {
    0: "Recibo",1:"Contado", 2:"Credito", 3: "Volantes", 4: "Tarjetas", 5:"Grifas", 6:"Hoja carta", 7: "Sobres", 8:"Planillas", 9:"Entradas/Bonos", 10:"Etiquetas", 11:"Otros"
};
$scope.selected2 = {value : $scope.workType[0]};

$scope.workPapel = {
    1:"Autocopiante", 2:"Oficio", 3:"Otro"
};
$scope.selected3 = {value: $scope.workPapel[0]};

var obj = [];
$scope.saveForm = function (formObj){
    formObj.estado = $scope.selected.value.value;
    formObj.tipo   = $scope.selected2.value.value;
    formObj.papel  = $scope.selected3.value.value;
    formObj.filter = "saveForm";
    formObj.idCliente = $scope.cliente.id;
    resultServices.saveForm(formObj, function(result, err){
        alert("Trabajo agregado correctamente");
    });
}
$scope.listWork = function (id){
    obj = {
        id: $rootScope.obj.id,
        filter: "listWork"
    }
    resultServices.getWorkById(obj, function(result, err){
        $scope.works = result.data;
        for ( var i = 1; i < $scope.wroks.lenght; i++) {
            $scope.works.count += i;
        }
    })
    ngDialog.open({
        template: 'components/result/views/tableWorks.html', 
        className: 'ngdialog-theme-default',
        scope: $scope,
        data: {
            confirm: function(obj){

            }
        }
    })
}  
}]);