app.controller('DbCtrl', ['$scope', '$rootScope', 'resultServices', 'ngDialog', '$state',  function($scope, $rootScope, resultServices, ngDialog, $state) {

    $scope.title = "Impregraf Database;";

    $scope.resultados = [];

    resultServices.getClients(function(result, err){
        if (!err) {
            $scope.resultados = result;
        } else {
            console.log("Error al Traer los Clientes.");
        }
    });

    $scope.delete = function(x){
        var obj ={
            id    : x.id,
            filter: "delete"
        }
        resultServices.delete(obj, function(result){
            alert("Desea eliminar al Cliente " + x.nombre);
            $state.reload();
        })
    };

    $scope.edit = function(x){
        $scope.info = x;
        ngDialog.open({
            template: 'components/result/views/editClientForm.html', 
            className: 'ngdialog-theme-default',
            scope: $scope,
            data: {
                confirm: function(obj){
                    obj.id     = $scope.info.id;
                    obj.filter = "edit";
                    resultServices.edit(obj, function(result){
                        $scope.action = "Cliente "+$scope.info.nombre+" editado con éxito";
                        ngDialog.close();
                        resultServices.getClients(function(result, err){
                            $scope.resultados = result;
                        })
                    });
                }
            }
        })
    }

    $scope.newClient = function(){
        ngDialog.open({
            template: 'components/result/views/newClientForm.html',
            className: 'ngdialog-theme-default',
            scope: $scope,
            data: {
                confirm: function(obj) {
                    obj.filter = "new";
                    resultServices.new(obj, function(result, err){
                        $scope.action = "Cliente " + obj.nombre + " añadido con éxito";
                        resultServices.getClients(function(result, err){
                            ngDialog.close();
                            $scope.resultados = result;
                        })
                    })
                }
            }
        })
    }

}]);