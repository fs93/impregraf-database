var app = angular.module('app', ['ui.router', 'angular-toArrayFilter', 'ngDialog', 'ui.select', 'ngSanitize']);

app.config(['$stateProvider', '$urlRouterProvider', '$qProvider', function($stateProvider, $urlRouterProvider, $qProvider) {

    $qProvider.errorOnUnhandledRejections(false);

    console.log("$stateProvider: ", $stateProvider)

    var resultState = {

        name: 'result',
        url: '/',
        templateUrl: 'components/result/views/resultView.html',
        controller: 'DbCtrl'

    }

    var resultFormState = {
        name: 'resultForm',
        url: '/clientForm',
        templateUrl: 'components/result/views/clientFormView.html',
        controller: 'formCtrl'
    }

    $stateProvider.state(resultState);
    $stateProvider.state(resultFormState);

    $urlRouterProvider.otherwise('/');

}]);

app.controller('DbCtrl', ['$scope', '$rootScope', function($scope, $rootScope) {

    $rootScope.state = "result";

    $scope.changeState = function(state) {
        console.log("trigger state ::", state);
        $rootScope.state = state;
    }


}]);