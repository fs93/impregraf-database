<?php

class connection{
	protected $connection;
	protected $servername = "localhost";
	protected $username   = "root";
    protected $password   = "";
    protected $database   = "impregraf";

	public function conect(){
		$this->connection = new mysqli($this->servername,$this->username,$this->password,$this->database, '3306');
		$this->connection->set_charset("utf8");
		return $this->connection;
	}


	public function query($sql){
		$resultado = mysqli_query($sql) or die('Consulta fallida: ' . mysqli_error($sql));
		$datos = "";
		//guardamos en un array
		while($fila =  mysqli_fetch_array($resultado)){
			$datos[] = $fila;
		}    	
		// Liberar resultados
		mysqli_free_result($resultado);

		return $datos;
	}

	public function disconect(){
		mysqli_close($this->connection);
	}

}

?>