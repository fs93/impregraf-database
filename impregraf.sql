-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 02-01-2020 a las 04:20:15
-- Versión del servidor: 5.7.26
-- Versión de PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `impregraf`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE IF NOT EXISTS `clientes` (
  `nombre` varchar(100) NOT NULL,
  `rut` bigint(200) NOT NULL,
  `mail` varchar(110) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` int(100) NOT NULL,
  `id` int(255) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`nombre`, `rut`, `mail`, `direccion`, `telefono`, `id`) VALUES
('Impregraf', 211003420017, 'impregraf@adinet.com.uy', 'blandengues 1727', 22030883, 1),
('Federico', 21212112, 'feds.fs93@gmail.com', 'Ana monterroso 2080', 92770294, 13),
('duende', 0, 'duende@duende', 'duende', 123, 23);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadotrabajo`
--

DROP TABLE IF EXISTS `estadotrabajo`;
CREATE TABLE IF NOT EXISTS `estadotrabajo` (
  `estado` text NOT NULL,
  `facturacion` int(12) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajoanterior`
--

DROP TABLE IF EXISTS `trabajoanterior`;
CREATE TABLE IF NOT EXISTS `trabajoanterior` (
  `numeracion` varchar(200) NOT NULL,
  `tipo` text NOT NULL,
  `papel` varchar(200) NOT NULL,
  `vias` int(10) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `vencimiento` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
